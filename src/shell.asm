.data
    $LC0: .asciiz "\n\nPlease choose one of the below options;\n"
    exitMessage: .asciiz "Goodbye :)\n"
    p1: .asciiz "ShowPrimes.asm"
    p2: .asciiz "Factorize.asm"
    p3: .asciiz "BubbleSort.asm"
    $LC1: .asciiz " 1.ShowPrimes.asm \n 2.Factorize.asm \n 3.BubbleSort.asm \n 4.Exit\n"
    $LC2: .asciiz "Choice:"
    $LC3: .asciiz "Please provide number between 1 and 4 \n\n"

.text
.globl main
main:
        addiu   $sp,$sp,-32
        sw      $31,28($sp)
        sw      $fp,24($sp)
        move    $fp,$sp
menu:
        jal     read_command
        nop
        move    $2,$0
        addi $v0, $s4,0
        addi $s0,$zero,1
        beq $v0,$s0,prog1
        addi $s0,$zero,2
        beq $v0,$s0,prog2
        addi $s0,$zero,3
        beq $v0,$s0,prog3
        addi $s0,$zero,4
        beq $v0,$s0,exitProgram

prog1:
        la $a0, p1
        li $v0, 18
        syscall
        j menu

prog2:
        la $a0, p2
        li $v0, 18
        syscall
        j menu

prog3:
        la $a0, p3
        li $v0, 18
        syscall
        j menu

exitProgram:
        move    $sp,$fp
        lw      $31,28($sp)
        lw      $fp,24($sp)
        addiu   $sp,$sp,32
        j       $31
        nop
read_command:
        addiu   $sp,$sp,-24
        sw      $fp,20($sp)
        move    $fp,$sp
$L6:
        sw $0,8($fp)
        la $a0, $LC0
        li $v0, 4
        syscall
        la $a0, $LC1
        li $v0, 4
        syscall

choiceLabel:
        la $a0, $LC2
        li $v0, 4
        syscall

        li $v0, 5
        syscall

        add $2,$zero,$v0

        addi $t1,$zero,1
        addi $t2,$zero,4

        slt $t0,$2,$t1
        beq $t0,$t1,provide

        slt $t0,$t2,$2
        beq $t0,$t1,provide
        j pass

 provide:
        la $a0, $LC3
        li $v0, 4
        syscall
        j choiceLabel
pass:
        add $s4,$zero,$2
        lw      $2,8($fp)
        move    $sp,$fp
        lw      $fp,20($sp)
        addiu   $sp,$sp,24
        j       $31
        nop