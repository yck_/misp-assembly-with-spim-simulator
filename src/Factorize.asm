# Yusuf Can Kan

.data
	input: .asciiz "Provide an integer:"
    error: .asciiz "Please provide a positive integer \n"
    space: .asciiz " "
    str1: .asciiz "factors of "
    str2: .asciiz " is: "
    nl: .asciiz "\n"

.text
.globl main
main:
    li $v0,4 #set print_string
    la $a0,input
    syscall

    # Read the input
    li $v0,5 #assign read_int for size of array
    syscall
    add $s0,$zero,$v0 

    addi $t7,$zero,1
    slt $t6,$s0,$t7

    addi $t7,$zero,0
    beq $t6,$t7,noerror
    
    li $v0,4 #set print_string
    la $a0,error
    syscall


    li $v0, 10 # System call code for exit
    syscall

noerror:
    # print "factors of"
	li $v0,4 #set print_string
	la $a0,str1
	syscall

    # print x input value
    addi $a0,$s0,0
    li $v0,1
	syscall

    # print "is"
	li $v0,4 #set print_string
	la $a0,str2
	syscall

    addi $s1,$zero,1
    
loop:
    div $s0, $s1
	mfhi $t0# take remainder
	beq $t0,$zero,printFactor # if remainder is zero it is factor

    j notPrint

printFactor:
    addi $a0,$s1,0
    li $v0,1
	syscall

	li $v0,4 #set print_string
	la $a0,space
	syscall

notPrint:
    beq $s1,$s0,exit
    addi $s1,$s1,1
    j loop

exit:
    # print new line
	li $v0,4 #set print_string
	la $a0,nl
	syscall

    li $v0, 10 # System call code for exit
    syscall