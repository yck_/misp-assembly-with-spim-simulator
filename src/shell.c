/*
* All the printf and scanf statements are changed in asm code.
* The asm file generated from https://godbolt.org.
*
*
*
*/


#include<stdio.h>
#define TRUE 1

int main(){

    /*I WROTE CREATE PROCESS (MY SYSCALL) INSTEAD OF FORK. YOU CAN RUN shell.asm file for program.

    */
    while(TRUE){ /* repeat forever */

        int choice=read_command(); /* read input from terminal */

        switch(choice){
            case 1:
                __asm__ ( "la $a0, p1\n\t"
                        "li $v0, 18\n\t"
                        "syscall\n\t");
                break;
            case 2:
                __asm__ ( "la $a0, p2\n\t"
                    "li $v0, 18\n\t"
                    "syscall\n\t");
                break;
            case 3:
                __asm__ ( "la $a0, p3\n\t"
                    "li $v0, 18\n\t"
                    "syscall\n\t");
                break;
            case 4:
                exit(0);
                break;
            default:
                printf("Unkown input\n");
        }
    }
}

int read_command(){
    char str1[]="\n\nPlease choose one of the below options;\n";
    char str2[]="1.ShowPrimes.asm \n 2.Factorize.asm \n 3.BubbleSort.asm \n 4.Exit\n";
    char str3[]="Choice:";
    char str4[]="Please provide number between 1 and 4 \n\n:";

    while(TRUE){
        int num=0;
        printf("%s",str1);
        printf("%s",str2);
        printf("%s",str3);
        scanf("%d",&num);
        if(num<1 || num>4){
            printf("Please provide number between 1 and 4 \n\n:");
        }
        else{
            return num;
        }
    }
}