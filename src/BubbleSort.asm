# Yusuf Can Kan

.data
    my_arr: .space 1024 # allocate 256 word size for array
    result: .asciiz "Result Array:"
    str7: .asciiz "Provide your array values:\n"
    str5: .asciiz "Provide size of array:"
    space: .asciiz " "
    nl: .asciiz "\n \n"

.text
.globl main
main:
	li $v0,4 # set print_string
	la $a0,str5
	syscall

	li $v0,5 # assign read_int for size of array
	syscall
	add $s0,$zero,$v0

    li $v0,4 # set print_string
	la $a0,str7
	syscall

    li $t0,0 # assign t0=0 for iterating array.
    li $t1,0 # control size statement

read_array: # reads the user input
	li $v0,5 # assign read_int for target number
	syscall
	sw $v0,my_arr($t0) #store target number to in s1 register
	addi $t0,$t0,4
	addi $t1,$t1,1
	beq $t1,$s0,leave_read
	j read_array

leave_read:
    # length
    addi $s1,$s0,0
    addi $s2,$s1,0
    li $s3,0

    addi $s7,$zero,1
    beq $s1,$s7,arrLenOne



loop1:
    addi $s4,$zero,0
    la $s0, my_arr # $s0 = array address

    sub $t1,$s1,$s3
    subu $t1,$t1,1

loop2:
    sll $t2,$s4,2
    # t2=arr[i]
    add $t0,$s0,$t2
    lw $t2,0($t0)
    
    # t4=arr[i+1]
    addi $t3,$t0,4
    lw $t4,0($t3)

    # -if t4>t2
    slt $t6,$t2,$t4

    addi $t7,$zero,1
    beq $t6,$t7,noswap

    # makes swap
    sw $t4,0($t0)
    sw $t2,0($t3)

noswap:
    addi $s4,$s4,1 # increase counter of loop

    slt $t5,$t1,$s4

    beq $t5,$t7,exit


    beq $s4,$t1,exitloop2 # if inner loop end
    j loop2 




exitloop2:
    addi $s3,$s3,1
    beq $s3,$s1,exit
    j loop1


exit:
	li $v0,4 #set print_string
	la $a0,result
	syscall

    la $s0, my_arr # $s0 = array address

    addi $t3,$zero,0

loopPrint:
    sll $t1,$t3,2

    # t2=arr[i]
    add $t0,$s0,$t1
    lw $a0,0($t0)

    addi $v0,$zero,1
    syscall

	li $v0,4 #set print_string
	la $a0,space
	syscall

    addi $t3,$t3,1

    beq $t3,$s1,exitProgram
    j loopPrint






exitProgram:
	li $v0,4 #set print_string
	la $a0,nl
	syscall

    li $v0, 10 # System call code for exit
    syscall



arrLenOne:
	li $v0,4 #set print_string
	la $a0,result
	syscall

    lw $a0,my_arr
    li $v0,1
    syscall
    j exitProgram