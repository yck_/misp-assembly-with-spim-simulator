# Yusuf Can Kan

.data
	nl: .asciiz "\n"
	ifPrime: .asciiz " prime\n"

.text
.globl main
main:
	# range is 0 - 1000
	li $s1,0
	li $s2,1001

loop1:
	addi $a0,$s1,0

	jal isPrime

	addi $a1,$v0,0 # take result of isPrime
	addi $a0,$s1,0 # take current loop number

	jal print  # print result while checking return value of isPrime
	addi $s1,$s1,1
	beq $s1,$s2,leave_while # exit condition
	j loop1




print:
	addi $t2,$zero,1
	beq $a1,$t2,printPrime
	li $v0,1
	syscall

	li $v0,4 #set print_string
	la $a0,nl
	syscall
	jr $ra

printPrime:
	li $v0,1
	syscall

	li $v0,4 #set print_string
	la $a0,ifPrime
	syscall

	jr $ra




# End of program
leave_while:
	li $v0, 10 # System call code for exit
	syscall




# if given number is prime it returns 1 else 0
isPrime:
	li $t1,2 # lover range is 2
	add $t2,$zero,$a0

	# if num is smaller than 2 it is not prime
	slt $t3,$t2,$t1
	addi $t4,$zero,1
	beq $t3,$t4,false

loop2:
	# if divider is equal itself it is prime
	beq $t1,$t2,true
	div $t2, $t1
	mfhi $t3 # take remainder
	beq $t3,$zero,false # if remainder is zero it is not prime
	addi $t1,$t1,1
	j loop2

# if prime
false:
	li $v0,0
	jr $ra

# if not prime
true:
	li $v0,1
	jr $ra

